﻿using EntityFrameworkBs.Common.DTOs.Abstraction;
using System;

namespace EntityFrameworkBs.Common.DTOs
{
    public class TeamDto : BaseEntityDto
    {
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }
    }
}
