﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace EntityFrameworkBs.DAL.Entities.Comparers
{
    public class TeamsEqualityComparer : IEqualityComparer<Team>
    {
        public bool Equals(Team x, Team y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] Team obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
