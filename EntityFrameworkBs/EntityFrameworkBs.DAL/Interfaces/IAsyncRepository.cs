﻿using EntityFrameworkBs.DAL.Entities.Abstraction;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EntityFrameworkBs.DAL.Interfaces
{
    public interface IAsyncRepository<T> : IRepository<T> where T : BaseEntity<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetByAsync(Func<T, bool> filter);
        Task<T> GetByIdAsync(int id);
        Task<T> UpdateAsync(T entity);
        Task DeleteAsync(int id);
        Task<T> CreateAsync(T entity);
    }
}
