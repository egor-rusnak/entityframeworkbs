﻿using System.Threading.Tasks;

namespace EntityFrameworkBs.Application.Commands
{
    public interface ICommand
    {
        string Description { get; set; }
        Task Execute();
    }
}
