﻿using EntityFrameworkBs.Common.DTOs;
using EntityFrameworkBs.Common.DTOs.Project;
using EntityFrameworkBs.Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EntityFrameworkBs.UI.Model.Interfaces
{
    public interface IProjectService : IDisposable
    {
        Task<IEnumerable<KeyValuePair<UserDto, List<TaskDto>>>> ExecuteQueryFive();
        Task<IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>> ExecuteQueryFour();
        Task<Dictionary<ProjectDto, int>> ExecuteQueryOne(int userId);
        Task<IEnumerable<ProjectInfoDto>> ExecuteQuerySeven();
        Task<UserInfoDto> ExecuteQuerySix(int userId);
        Task<IEnumerable<KeyValuePair<int, string>>> ExecuteQueryThree(int userId);
        Task<IEnumerable<TaskDto>> ExecuteQueryTwo(int userId);

    }
}