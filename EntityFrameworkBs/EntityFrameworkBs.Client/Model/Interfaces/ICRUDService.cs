﻿using EntityFrameworkBs.Common.DTOs.Abstraction;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EntityFrameworkBs.UI.Model.Interfaces
{
    public interface ICRUDService<T> where T : BaseEntityDto
    {
        Task<T> Create(T entity);
        Task<bool> Update(T entity);
        Task<IEnumerable<T>> GetAll();
        Task<bool> Delete(int id);
    }
}
