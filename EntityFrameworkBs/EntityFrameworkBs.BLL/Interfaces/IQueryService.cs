﻿using EntityFrameworkBs.Common.DTOs;
using EntityFrameworkBs.Common.DTOs.Project;
using EntityFrameworkBs.Common.DTOs.User;
using System.Collections.Generic;

namespace EntityFrameworkBs.BLL.Interfaces
{
    public interface IQueryService
    {
        IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>> ExecuteQueryFive();
        IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour();
        Dictionary<ProjectDto, int> ExecuteQueryOne(int userId);
        IEnumerable<ProjectInfoDto> ExecuteQuerySeven();
        UserInfoDto ExecuteQuerySix(int userId);
        IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId);
        IEnumerable<TaskDto> ExecuteQueryTwo(int userId);
    }
}