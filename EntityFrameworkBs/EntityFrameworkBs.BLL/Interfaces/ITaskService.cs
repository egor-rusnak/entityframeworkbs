﻿using EntityFrameworkBs.Common.DTOs;
using System.Collections.Generic;

namespace EntityFrameworkBs.BLL.Interfaces
{
    public interface ITaskService
    {
        TaskDto AddTask(TaskDto task);
        IEnumerable<TaskDto> GetAllTasks();
        TaskDto GetById(int id);
        void RemoveTask(int id);
        TaskDto UpdateTask(TaskDto task);
    }
}