﻿using EntityFrameworkBs.Common.DTOs.User;
using System.Collections.Generic;

namespace EntityFrameworkBs.BLL.Interfaces
{
    public interface IUserService
    {
        UserDto AddUser(UserDto user);
        IEnumerable<UserDto> GetAllUsers();
        UserDto GetById(int id);
        void RemoveUser(int id);
        UserDto UpdateUser(UserDto user);
    }
}