﻿using AutoMapper;
using EntityFrameworkBs.BLL.Interfaces;
using EntityFrameworkBs.BLL.Services.Abstraction;
using EntityFrameworkBs.Common.DTOs.Project;
using EntityFrameworkBs.DAL.Context;
using EntityFrameworkBs.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntityFrameworkBs.BLL.Services
{
    public class ProjectService : BaseSerivce, IProjectService
    {
        public ProjectService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }


        public ProjectDto AddProject(ProjectDto newProject)
        {
            var projectEntity = _mapper.Map<Project>(newProject);

            _context.Projects.Add(projectEntity);

            _context.SaveChanges();

            return _mapper.Map<ProjectDto>(GetProject(projectEntity.Id));
        }

        public void RemoveProject(int id)
        {
            var projectEntity = _context.Projects.FirstOrDefault(p => p.Id == id); //not using getproject because of includes

            if (projectEntity == null) throw new ArgumentException("No entity with this id");

            _context.Remove(projectEntity);
            _context.SaveChanges();
        }

        public ProjectDto UpdateProject(ProjectDto project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            _context.Projects.Update(projectEntity);

            _context.SaveChanges();

            return _mapper.Map<ProjectDto>(GetProject(projectEntity.Id));
        }

        public IEnumerable<ProjectDto> GetAllProjects()
        {
            var elems = GetProjects().ToList();
            return _mapper.Map<IEnumerable<ProjectDto>>(elems);
        }

        public ProjectDto GetById(int id)
        {
            var projectEntity = GetProject(id);

            if (projectEntity == null) throw new ArgumentException("No entity with this id");

            return _mapper.Map<ProjectDto>(projectEntity);
        }

        private IQueryable<Project> GetProjects()
        {
            return _context.Projects
                .Include(p => p.Team)
                .Include(p => p.Tasks).ThenInclude(t => t.Performer)
                .Include(p => p.Author);
        }

        private Project GetProject(int id)
        {
            return GetProjects().FirstOrDefault(p => p.Id == id);
        }
    }
}
