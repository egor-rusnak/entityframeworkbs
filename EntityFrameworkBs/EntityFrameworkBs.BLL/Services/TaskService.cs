﻿using AutoMapper;
using EntityFrameworkBs.BLL.Interfaces;
using EntityFrameworkBs.BLL.Services.Abstraction;
using EntityFrameworkBs.Common.DTOs;
using EntityFrameworkBs.DAL.Context;
using EntityFrameworkBs.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntityFrameworkBs.BLL.Services
{
    public class TaskService : BaseSerivce, ITaskService
    {
        public TaskService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public TaskDto AddTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            _context.Tasks.Add(taskEntity);

            _context.SaveChanges();
            return _mapper.Map<TaskDto>(GetTask(taskEntity.Id));
        }

        public TaskDto UpdateTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            _context.Tasks.Update(taskEntity);

            _context.SaveChanges();
            return _mapper.Map<TaskDto>(GetTask(taskEntity.Id));
        }

        public void RemoveTask(int id)
        {
            var taskEntity = GetTask(id);

            if (taskEntity == null) throw new ArgumentException("No entity with this id!");

            _context.Tasks.Remove(taskEntity);

            _context.SaveChanges();
        }

        public IEnumerable<TaskDto> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(GetTasks());
        }

        public TaskDto GetById(int id)
        {
            var taskEntity = GetTask(id);

            if (taskEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<TaskDto>(taskEntity);
        }

        private IQueryable<Task> GetTasks()
        {
            return _context.Tasks.Include(t => t.Performer);
        }
        private Task GetTask(int id)
        {
            return GetTasks().FirstOrDefault(t => t.Id == id);
        }

    }
}
