﻿using AutoMapper;
using EntityFrameworkBs.BLL.Interfaces;
using EntityFrameworkBs.BLL.Services.Abstraction;
using EntityFrameworkBs.Common.DTOs;
using EntityFrameworkBs.DAL.Context;
using EntityFrameworkBs.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntityFrameworkBs.BLL.Services
{
    public class TeamService : BaseSerivce, ITeamService
    {
        public TeamService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public TeamDto AddTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);

            _context.Teams.Add(teamEntity);

            _context.SaveChanges();
            return _mapper.Map<TeamDto>(GetTeam(teamEntity.Id));
        }

        public TeamDto UpdateTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);

            _context.Teams.Update(teamEntity);

            _context.SaveChanges();
            return _mapper.Map<TeamDto>(GetTeam(teamEntity.Id));
        }

        public void RemoveTeam(int id)
        {
            var teamEntity = GetTeam(id);

            if (teamEntity == null) throw new ArgumentException("No entity with this id!");

            _context.Teams.Remove(teamEntity);

            _context.SaveChanges();
        }

        public IEnumerable<TeamDto> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(GetTeams());
        }

        public TeamDto GetById(int id)
        {
            var teamEntity = GetTeam(id);

            if (teamEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<TeamDto>(teamEntity);
        }

        private IQueryable<Team> GetTeams()
        {
            return _context.Teams.Include(t => t.Members);
        }

        private Team GetTeam(int id)
        {
            return GetTeams().FirstOrDefault(t => t.Id == id);
        }
    }
}
