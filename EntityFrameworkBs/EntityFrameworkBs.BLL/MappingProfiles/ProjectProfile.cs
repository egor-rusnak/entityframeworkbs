﻿using AutoMapper;
using EntityFrameworkBs.Common.DTOs.Project;
using EntityFrameworkBs.DAL.Entities;

namespace EntityFrameworkBs.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>();
        }
    }
}
