﻿using AutoMapper;
using EntityFrameworkBs.Common.DTOs;
using EntityFrameworkBs.DAL.Entities;

namespace EntityFrameworkBs.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
        }
    }
}
