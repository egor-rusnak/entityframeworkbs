﻿using AutoMapper;
using EntityFrameworkBs.Common.DTOs.User;
using EntityFrameworkBs.DAL.Entities;

namespace EntityFrameworkBs.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}
