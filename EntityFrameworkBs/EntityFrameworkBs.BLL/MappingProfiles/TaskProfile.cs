﻿using AutoMapper;
using EntityFrameworkBs.Common.DTOs;
using EntityFrameworkBs.DAL.Entities;


namespace EntityFrameworkBs.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>();
        }
    }
}
